# Shared #

### What is this repository for? ###

This is a small test balloon to see how git submodules are working. This is the submodule which will be included by [Project 1](../../../gsub-project1/) and  [Project 2](../../../gsub-project2/)

### How do I get set up? ###

Check out all repositories of this Bitbucket project.

### Who do I talk to? ###

There is no support on this project

### License ###

This project is published under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

